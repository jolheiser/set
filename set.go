package set

// IntSet is a set of int
type IntSet map[int]struct{}

// Add adds a new key to the IntSet
// If the key exists, this is a no-op
func (s IntSet) Add(keys ...int) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the IntSet
// If the key does not exist, this is a no-op
func (s IntSet) Remove(keys ...int) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the IntSet
func (s IntSet) Has(key int) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the IntSet
func (s IntSet) Slice() []int {
	sl := make([]int, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two IntSet
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s IntSet) Union(other IntSet) IntSet {
	u := NewIntSetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two IntSet
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s IntSet) Intersect(other IntSet) IntSet {
	i := NewIntSet()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this IntSet against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s IntSet) Compliment(other IntSet) IntSet {
	c := NewIntSet()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewIntSet returns a newly made IntSet
func NewIntSet() IntSet {
	return make(IntSet)
}

// IntSetFromSlice returns a new IntSet from an existing slice
func NewIntSetFromSlice(slice []int) IntSet {
	set := NewIntSet()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Int8Set is a set of int8
type Int8Set map[int8]struct{}

// Add adds a new key to the Int8Set
// If the key exists, this is a no-op
func (s Int8Set) Add(keys ...int8) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Int8Set
// If the key does not exist, this is a no-op
func (s Int8Set) Remove(keys ...int8) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Int8Set
func (s Int8Set) Has(key int8) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Int8Set
func (s Int8Set) Slice() []int8 {
	sl := make([]int8, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Int8Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Int8Set) Union(other Int8Set) Int8Set {
	u := NewInt8SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Int8Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Int8Set) Intersect(other Int8Set) Int8Set {
	i := NewInt8Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Int8Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Int8Set) Compliment(other Int8Set) Int8Set {
	c := NewInt8Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewInt8Set returns a newly made Int8Set
func NewInt8Set() Int8Set {
	return make(Int8Set)
}

// Int8SetFromSlice returns a new Int8Set from an existing slice
func NewInt8SetFromSlice(slice []int8) Int8Set {
	set := NewInt8Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Int16Set is a set of int16
type Int16Set map[int16]struct{}

// Add adds a new key to the Int16Set
// If the key exists, this is a no-op
func (s Int16Set) Add(keys ...int16) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Int16Set
// If the key does not exist, this is a no-op
func (s Int16Set) Remove(keys ...int16) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Int16Set
func (s Int16Set) Has(key int16) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Int16Set
func (s Int16Set) Slice() []int16 {
	sl := make([]int16, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Int16Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Int16Set) Union(other Int16Set) Int16Set {
	u := NewInt16SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Int16Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Int16Set) Intersect(other Int16Set) Int16Set {
	i := NewInt16Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Int16Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Int16Set) Compliment(other Int16Set) Int16Set {
	c := NewInt16Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewInt16Set returns a newly made Int16Set
func NewInt16Set() Int16Set {
	return make(Int16Set)
}

// Int16SetFromSlice returns a new Int16Set from an existing slice
func NewInt16SetFromSlice(slice []int16) Int16Set {
	set := NewInt16Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Int32Set is a set of int32
type Int32Set map[int32]struct{}

// Add adds a new key to the Int32Set
// If the key exists, this is a no-op
func (s Int32Set) Add(keys ...int32) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Int32Set
// If the key does not exist, this is a no-op
func (s Int32Set) Remove(keys ...int32) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Int32Set
func (s Int32Set) Has(key int32) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Int32Set
func (s Int32Set) Slice() []int32 {
	sl := make([]int32, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Int32Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Int32Set) Union(other Int32Set) Int32Set {
	u := NewInt32SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Int32Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Int32Set) Intersect(other Int32Set) Int32Set {
	i := NewInt32Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Int32Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Int32Set) Compliment(other Int32Set) Int32Set {
	c := NewInt32Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewInt32Set returns a newly made Int32Set
func NewInt32Set() Int32Set {
	return make(Int32Set)
}

// Int32SetFromSlice returns a new Int32Set from an existing slice
func NewInt32SetFromSlice(slice []int32) Int32Set {
	set := NewInt32Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Int64Set is a set of int64
type Int64Set map[int64]struct{}

// Add adds a new key to the Int64Set
// If the key exists, this is a no-op
func (s Int64Set) Add(keys ...int64) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Int64Set
// If the key does not exist, this is a no-op
func (s Int64Set) Remove(keys ...int64) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Int64Set
func (s Int64Set) Has(key int64) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Int64Set
func (s Int64Set) Slice() []int64 {
	sl := make([]int64, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Int64Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Int64Set) Union(other Int64Set) Int64Set {
	u := NewInt64SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Int64Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Int64Set) Intersect(other Int64Set) Int64Set {
	i := NewInt64Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Int64Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Int64Set) Compliment(other Int64Set) Int64Set {
	c := NewInt64Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewInt64Set returns a newly made Int64Set
func NewInt64Set() Int64Set {
	return make(Int64Set)
}

// Int64SetFromSlice returns a new Int64Set from an existing slice
func NewInt64SetFromSlice(slice []int64) Int64Set {
	set := NewInt64Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// UintSet is a set of uint
type UintSet map[uint]struct{}

// Add adds a new key to the UintSet
// If the key exists, this is a no-op
func (s UintSet) Add(keys ...uint) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the UintSet
// If the key does not exist, this is a no-op
func (s UintSet) Remove(keys ...uint) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the UintSet
func (s UintSet) Has(key uint) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the UintSet
func (s UintSet) Slice() []uint {
	sl := make([]uint, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two UintSet
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s UintSet) Union(other UintSet) UintSet {
	u := NewUintSetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two UintSet
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s UintSet) Intersect(other UintSet) UintSet {
	i := NewUintSet()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this UintSet against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s UintSet) Compliment(other UintSet) UintSet {
	c := NewUintSet()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewUintSet returns a newly made UintSet
func NewUintSet() UintSet {
	return make(UintSet)
}

// UintSetFromSlice returns a new UintSet from an existing slice
func NewUintSetFromSlice(slice []uint) UintSet {
	set := NewUintSet()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Uint8Set is a set of uint8
type Uint8Set map[uint8]struct{}

// Add adds a new key to the Uint8Set
// If the key exists, this is a no-op
func (s Uint8Set) Add(keys ...uint8) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Uint8Set
// If the key does not exist, this is a no-op
func (s Uint8Set) Remove(keys ...uint8) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Uint8Set
func (s Uint8Set) Has(key uint8) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Uint8Set
func (s Uint8Set) Slice() []uint8 {
	sl := make([]uint8, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Uint8Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Uint8Set) Union(other Uint8Set) Uint8Set {
	u := NewUint8SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Uint8Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Uint8Set) Intersect(other Uint8Set) Uint8Set {
	i := NewUint8Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Uint8Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Uint8Set) Compliment(other Uint8Set) Uint8Set {
	c := NewUint8Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewUint8Set returns a newly made Uint8Set
func NewUint8Set() Uint8Set {
	return make(Uint8Set)
}

// Uint8SetFromSlice returns a new Uint8Set from an existing slice
func NewUint8SetFromSlice(slice []uint8) Uint8Set {
	set := NewUint8Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Uint16Set is a set of uint16
type Uint16Set map[uint16]struct{}

// Add adds a new key to the Uint16Set
// If the key exists, this is a no-op
func (s Uint16Set) Add(keys ...uint16) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Uint16Set
// If the key does not exist, this is a no-op
func (s Uint16Set) Remove(keys ...uint16) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Uint16Set
func (s Uint16Set) Has(key uint16) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Uint16Set
func (s Uint16Set) Slice() []uint16 {
	sl := make([]uint16, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Uint16Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Uint16Set) Union(other Uint16Set) Uint16Set {
	u := NewUint16SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Uint16Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Uint16Set) Intersect(other Uint16Set) Uint16Set {
	i := NewUint16Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Uint16Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Uint16Set) Compliment(other Uint16Set) Uint16Set {
	c := NewUint16Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewUint16Set returns a newly made Uint16Set
func NewUint16Set() Uint16Set {
	return make(Uint16Set)
}

// Uint16SetFromSlice returns a new Uint16Set from an existing slice
func NewUint16SetFromSlice(slice []uint16) Uint16Set {
	set := NewUint16Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Uint32Set is a set of uint32
type Uint32Set map[uint32]struct{}

// Add adds a new key to the Uint32Set
// If the key exists, this is a no-op
func (s Uint32Set) Add(keys ...uint32) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Uint32Set
// If the key does not exist, this is a no-op
func (s Uint32Set) Remove(keys ...uint32) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Uint32Set
func (s Uint32Set) Has(key uint32) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Uint32Set
func (s Uint32Set) Slice() []uint32 {
	sl := make([]uint32, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Uint32Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Uint32Set) Union(other Uint32Set) Uint32Set {
	u := NewUint32SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Uint32Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Uint32Set) Intersect(other Uint32Set) Uint32Set {
	i := NewUint32Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Uint32Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Uint32Set) Compliment(other Uint32Set) Uint32Set {
	c := NewUint32Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewUint32Set returns a newly made Uint32Set
func NewUint32Set() Uint32Set {
	return make(Uint32Set)
}

// Uint32SetFromSlice returns a new Uint32Set from an existing slice
func NewUint32SetFromSlice(slice []uint32) Uint32Set {
	set := NewUint32Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Uint64Set is a set of uint64
type Uint64Set map[uint64]struct{}

// Add adds a new key to the Uint64Set
// If the key exists, this is a no-op
func (s Uint64Set) Add(keys ...uint64) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Uint64Set
// If the key does not exist, this is a no-op
func (s Uint64Set) Remove(keys ...uint64) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Uint64Set
func (s Uint64Set) Has(key uint64) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Uint64Set
func (s Uint64Set) Slice() []uint64 {
	sl := make([]uint64, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Uint64Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Uint64Set) Union(other Uint64Set) Uint64Set {
	u := NewUint64SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Uint64Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Uint64Set) Intersect(other Uint64Set) Uint64Set {
	i := NewUint64Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Uint64Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Uint64Set) Compliment(other Uint64Set) Uint64Set {
	c := NewUint64Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewUint64Set returns a newly made Uint64Set
func NewUint64Set() Uint64Set {
	return make(Uint64Set)
}

// Uint64SetFromSlice returns a new Uint64Set from an existing slice
func NewUint64SetFromSlice(slice []uint64) Uint64Set {
	set := NewUint64Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Float32Set is a set of float32
type Float32Set map[float32]struct{}

// Add adds a new key to the Float32Set
// If the key exists, this is a no-op
func (s Float32Set) Add(keys ...float32) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Float32Set
// If the key does not exist, this is a no-op
func (s Float32Set) Remove(keys ...float32) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Float32Set
func (s Float32Set) Has(key float32) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Float32Set
func (s Float32Set) Slice() []float32 {
	sl := make([]float32, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Float32Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Float32Set) Union(other Float32Set) Float32Set {
	u := NewFloat32SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Float32Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Float32Set) Intersect(other Float32Set) Float32Set {
	i := NewFloat32Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Float32Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Float32Set) Compliment(other Float32Set) Float32Set {
	c := NewFloat32Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewFloat32Set returns a newly made Float32Set
func NewFloat32Set() Float32Set {
	return make(Float32Set)
}

// Float32SetFromSlice returns a new Float32Set from an existing slice
func NewFloat32SetFromSlice(slice []float32) Float32Set {
	set := NewFloat32Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// Float64Set is a set of float64
type Float64Set map[float64]struct{}

// Add adds a new key to the Float64Set
// If the key exists, this is a no-op
func (s Float64Set) Add(keys ...float64) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the Float64Set
// If the key does not exist, this is a no-op
func (s Float64Set) Remove(keys ...float64) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the Float64Set
func (s Float64Set) Has(key float64) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the Float64Set
func (s Float64Set) Slice() []float64 {
	sl := make([]float64, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two Float64Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s Float64Set) Union(other Float64Set) Float64Set {
	u := NewFloat64SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two Float64Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s Float64Set) Intersect(other Float64Set) Float64Set {
	i := NewFloat64Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this Float64Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s Float64Set) Compliment(other Float64Set) Float64Set {
	c := NewFloat64Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewFloat64Set returns a newly made Float64Set
func NewFloat64Set() Float64Set {
	return make(Float64Set)
}

// Float64SetFromSlice returns a new Float64Set from an existing slice
func NewFloat64SetFromSlice(slice []float64) Float64Set {
	set := NewFloat64Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}

// StringSet is a set of string
type StringSet map[string]struct{}

// Add adds a new key to the StringSet
// If the key exists, this is a no-op
func (s StringSet) Add(keys ...string) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the StringSet
// If the key does not exist, this is a no-op
func (s StringSet) Remove(keys ...string) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the StringSet
func (s StringSet) Has(key string) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the StringSet
func (s StringSet) Slice() []string {
	sl := make([]string, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two StringSet
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s StringSet) Union(other StringSet) StringSet {
	u := NewStringSetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two StringSet
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s StringSet) Intersect(other StringSet) StringSet {
	i := NewStringSet()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this StringSet against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s StringSet) Compliment(other StringSet) StringSet {
	c := NewStringSet()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// NewStringSet returns a newly made StringSet
func NewStringSet() StringSet {
	return make(StringSet)
}

// StringSetFromSlice returns a new StringSet from an existing slice
func NewStringSetFromSlice(slice []string) StringSet {
	set := NewStringSet()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}
