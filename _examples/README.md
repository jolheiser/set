# Examples

A small sample of examples

Given sets A and B:
- [compliment](compliment) returns a set of items that exist in B but not A
- [intersect](intersect) returns a set of items that exist in **both** A and B
- [union](union) returns a set of items aggregated from A and B

[set_slice](set_slice) returns a slice that only contains unique values