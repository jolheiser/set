package main

import (
	"fmt"
	"sort"

	"go.jolheiser.com/set"
)

func main() {
	// Slice of ints
	slice := []int{1, 1, 2, 3, 4, 3, 2, 5, 6, 7, 3, 6, 9, 8}

	// Convert it to a set, and then back into a slice
	unique := set.NewIntSetFromSlice(slice).Slice()

	// Sort (for consistency)
	sort.Ints(unique)

	// 1 2 3 4 5 6 7 8 9
	fmt.Println(unique)
}
