package main

import (
	"fmt"
	"go.jolheiser.com/set"
	"sort"
)

func main() {
	// Set 1
	set1 := set.NewStringSet()
	set1.Add("a", "b", "c")

	// Set 2
	set2 := set.NewStringSet()
	set2.Add("1", "2", "3")

	// Union
	set3 := set1.Union(set2)

	// Convert to slice and sort
	slice := set3.Slice()
	sort.Strings(slice)

	// 1 2 3 a b c
	fmt.Println(slice)
}
