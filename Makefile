GO ?= go

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: test
test:
	$(GO) test -race ./...

.PHONY: generate
generate:
	$(GO) run generate.go
