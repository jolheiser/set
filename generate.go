// +build generate

package main

import (
	"os"
	"strings"
	"text/template"
)

var types = []string{
	"int", "int8", "int16", "int32", "int64",
	"uint", "uint8", "uint16", "uint32", "uint64",
	"float32", "float64",
	"string",
}

func main() {
	setFi, err := os.Create("set.go")
	if err != nil {
		panic(err)
	}
	defer setFi.Close()

	if err := tmplSet.Execute(setFi, map[string]interface{}{
		"types": types,
	}); err != nil {
		panic(err)
	}
}

var tmplSet = template.Must(template.New("hashset").Funcs(funcMap).Parse(`package set
{{range $type := .types}}
// {{title $type}}Set is a set of {{$type}}
type {{title $type}}Set map[{{$type}}]struct{}

// Add adds a new key to the {{title $type}}Set
// If the key exists, this is a no-op
func (s {{title $type}}Set) Add(keys ...{{$type}}) {
	for _, key := range keys {
		s[key] = struct{}{}
	}
}

// Remove removes a key from the {{title $type}}Set
// If the key does not exist, this is a no-op
func (s {{title $type}}Set) Remove(keys ...{{$type}}) {
	for _, key := range keys {
		delete(s, key)
	}
}

// Has checks if the key exists in the {{title $type}}Set
func (s {{title $type}}Set) Has(key {{$type}}) bool {
	_, ok := s[key]
	return ok
}

// Slice returns a slice of the {{title $type}}Set
func (s {{title $type}}Set) Slice() []{{$type}} {
	sl := make([]{{$type}}, len(s))
	idx := 0
	for k := range s {
		sl[idx] = k
		idx++
	}
	return sl
}

// Union returns a union of two {{title $type}}Set
// https://en.wikipedia.org/wiki/Union_(set_theory)
func (s {{title $type}}Set) Union(other {{title $type}}Set) {{title $type}}Set {
	u := New{{title $type}}SetFromSlice(s.Slice())
	for k := range other {
		u.Add(k)
	}
	return u
}

// Intersect returns an intersection of two {{title $type}}Set
// https://en.wikipedia.org/wiki/Intersection_(set_theory)
func (s {{title $type}}Set) Intersect(other {{title $type}}Set) {{title $type}}Set {
	i := New{{title $type}}Set()
	for k := range s {
		if other.Has(k) {
			i.Add(k)
		}
	}
	return i
}

// Compliment returns a compliment of this {{title $type}}Set against another
// https://en.wikipedia.org/wiki/Complement_(set_theory)
func (s {{title $type}}Set) Compliment(other {{title $type}}Set) {{title $type}}Set {
	c := New{{title $type}}Set()
	for k := range other {
		if !s.Has(k) {
			c.Add(k)
		}
	}
	return c
}

// New{{title $type}}Set returns a newly made {{title $type}}Set
func New{{title $type}}Set() {{title $type}}Set {
	return make({{title $type}}Set)
}

// {{title $type}}SetFromSlice returns a new {{title $type}}Set from an existing slice
func New{{title $type}}SetFromSlice(slice []{{$type}}) {{title $type}}Set {
	set := New{{title $type}}Set()
	for _, s := range slice {
		set.Add(s)
	}
	return set
}
{{end}}`))

var funcMap = template.FuncMap{
	"title": strings.Title,
}
